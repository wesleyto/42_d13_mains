LAST_EX=7


### NORMINETTE ###
if [ "$#" -eq 1 ] && [ $1 == '-n' ];
then
	echo ===========================
	echo === Skipping Norminette ===
	echo ===========================
else
	echo ===========================
	echo ======= Norminette ========
	echo ===========================
	if ls ex*/*.c 1> /dev/null 2>&1; then
		norminette ex*/*.c
	fi
	if ls ex*/*.h 1> /dev/null 2>&1; then
		norminette ex*/*.h
	fi
fi
echo ...Done
echo


### COMPILING FILES ###
echo ===========================
echo ===== Test Compiling ======
echo ===========================
for num in $(seq -f "%02g" 0 $LAST_EX)
do
	if ls ex$num/*.c 1> /dev/null 2>&1; then
		gcc -Wall -Wextra -Werror -c ex$num/*.c
		rm *.o
	fi
done
echo ...Done
echo


### COPY OVER TEST FILES ###
echo ===========================
echo ====== Copying Files ======
echo ===========================
for num in $(seq -f "%02g" 0 $LAST_EX)
do
	cp -R ex$num/* TESTFILES/ex$num/
done
echo ...Done
echo


cd TESTFILES


### COMPILE WITH TEST FILES ###
echo ===========================
echo ===== Compiling Mains =====
echo ===========================
for num in $(seq -f "%02g" 0 $LAST_EX)
do
	if [ -e ex$num/main.c ] && [ ! -e ex$num/Makefile ] && [ ! -e ex$num/test.sh ]; then
		if ls ex$num/*.o 1> /dev/null 2>&1; then
			gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c ex$num/*.o
		else
			gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c
		fi
	fi
done
echo ...Done
echo


### RUN TEST FILES ###
for num in $(seq -f "%02g" 0 $LAST_EX)
do
	echo ===========================
	echo ====== Testing Ex $num ======
	echo ===========================
	if [ -e ex$num/main ]; then
		./ex$num/main
	elif [ -e ex$num/Makefile ]; then
		echo MANUALLY TEST USING make
	elif [ -e ex$num/test.sh ]; then
		echo MANUALLY TEST USING .sh SCRIPT
	else
		echo Nothing to test...
	fi
	echo
done

cd ..