/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "btree_apply_infix.c"

t_btree	*make_tree(void *data, int length);
void teardown(t_btree *root);
void print_tree(t_btree *root);

void	do_something(void *data)
{
	printf("%s ", data);
}

int main(void)
{
	t_btree *tree;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *str6 = "6";
	char *str7 = "7";
	char *str8 = "8";
	char *strs[] = {str0, str1, str2, str3, str4, str5,
					str6, str7, str8};
	tree = make_tree(strs, 9);
	print_tree(tree);
	printf("\nIn-Order:\t");
	btree_apply_infix(tree, &do_something);
	printf("\nExpecting:\t7 3 8 1 4 0 5 2 6");
	teardown(tree);
}