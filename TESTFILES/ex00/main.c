/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "btree_create_node.c"

int main(void)
{
	t_btree *node;
	t_btree *willy;
	char *str = "Success! Something";
	char *str2 = "Success! Something else";
	node = btree_create_node(str);
	willy = node;
	printf("\"%s\" is stored in the node\n", (*node).item);
	node = btree_create_node(str2);
	free(willy);
	printf("\"%s\" is stored in the node\n", (*node).item);
	free(node);
}
