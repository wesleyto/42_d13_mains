/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "btree_search_item.c"
#include <string.h>

t_btree	*make_tree(void *data, int length);
void teardown(t_btree *root);
void print_tree(t_btree *root);

void	do_something(void *data)
{
	printf("%s ", data);
}

int cmpf(void *d1, void *d2)
{
	return strcmp((char *)d1, (char *)d2);
}

int main(void)
{
	t_btree *tree;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *str6 = "6";
	char *str7 = "7";
	char *str8 = "8";
	char *str9 = "9";
	char *strs[] = {str0, str1, str2, str3, str4, str5, str6, str7};
	char *searches[] = {str0, str1, str2, str3, str4, str5,
					str6, str7, str8, str9};
	char *exps[] = {str0, str1, str2, str3, str4, str5,
					str6, str7, NULL, NULL};
	char *result;
	char *res;
	tree = make_tree(strs, 8);
	char *res_str = "%s || Got: %-6s || Exp: %-6s || Case: Search For \"%s\"\n";
	print_tree(tree);
	printf("\n");
	for (int i = 0; i < 10; i++)
	{
		result = btree_search_item(tree, searches[i], cmpf);
		res = (result ? strcmp(result, searches[i]) == 0 : result == exps[i]) ? "Success" : "Failure";
		printf(res_str, res, result, exps[i], searches[i]);
	}
	teardown(tree);
}