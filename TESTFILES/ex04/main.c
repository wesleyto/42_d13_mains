/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "btree_insert_data.c"
#include <string.h>

t_btree	*make_tree(void *data, int length);
void teardown(t_btree *root);
void print_tree(t_btree *root);

void	do_something(void *data)
{
	printf("%s ", data);
}

int cmpf(void *d1, void *d2)
{
	return strcmp((char *)d1, (char *)d2);
}

int main(void)
{
	t_btree *tree;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *str6 = "6";
	char *str7 = "7";
	char *str8 = "8";
	char *str9 = "9";
	char *strs[] = {str3, str1, str5, str0, str2, str4,
					str6};
	char *inserts[] = {str8, str9, str2, str0, str7};
	tree = make_tree(strs, 7);

	printf("\nBefore:\n");
	print_tree(tree);
	for (int i = 0; i < 5; i++)
	{
		printf("\nInserting \"%s\"", inserts[i]);
		btree_insert_data(&tree, inserts[i], &cmpf);
	}
	printf("\nAfter Insertion:\n");
	print_tree(tree);
	teardown(tree);
}