/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "btree_level_count.c"

t_btree	*make_tree(void *data, int length);
void teardown(t_btree *root);
void print_tree(t_btree *root);

int main(void)
{
	t_btree *tree;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *str6 = "6";
	char *str7 = "7";
	char *strs1[] = {str0};
	char *strs2[] = {str0, str1};
	char *strs3[] = {str0, str1, str2, str3, str4, str5};
	char *strs4[] = {str0, str1, str2, str3, str4, str5, str6, str7};
	char **s[] = {strs1, strs2, strs3, strs4};
	int len[] = {1, 2, 6, 8};
	int exps[] = {1, 2, 3, 4};
	int result;
	char *res;
	char *res_str = "%s || Got: %-6d || Exp: %-6d\n";
	for (int i = 0; i < 4; i++)
	{
		tree = make_tree(s[i], len[i]);
		result = btree_level_count(tree);
		res = result == exps[i] ? "Success" : "Failure";
		printf("\nTree:\n");
		print_tree(tree);
		printf(res_str, res, result, exps[i]);
		teardown(tree);
	}
}